<header class="text-center py-5">
    <h1 class="text-uppercase ls-2 fw-bold fs-2">
        <?= $page ?> List
    </h1>
</header>
<section class="products bg-white py-5">
    <div class="container">
        <div class="row">
            <?php for($i = 1; $i <= 12; $i++) :?>
            <div class="col-6 col-md-4 mb-4 g-2 g-sm-3">
                <a href="product-detail.php" class="text-decoration-none text-dark">
                    <div class="card product-card">
                    <!-- <div class="discount-sign">
                        <h6 class="discount">20<span>%</span></h6>
                        <p>Off</p>
                    </div> -->
                        <img class="img" src="/assets/img/genoa.jpg" class="card-img-top" alt="boboyuk" title="sisilia">
                        <div class="card-body text-center">
                            <h5 class="card-title product-title fw-normal">Milan</h5>
                            <p class="card-text product-price fw-medium">
                            <span class="lower-price">Rp 1.000.000</span> - 
                            <span class="lower-price">Rp 10.000.000</span>
                            </p>
                        </div>
                    </div>
                </a>
            </div>
            <?php endfor ?>
        </div>
    </div>
</section>