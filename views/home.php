<section id="slider">
  <!-- Slider main container -->
  <div class="swiper-container main-swiper">
    <!-- Additional required wrapper -->
    <div class="swiper-wrapper">
      <!-- Slides -->
      <?php for($i = 1; $i <= 3; $i++) :?>
        <div class="swiper-slide main-slide">
          <img class="bg-main-slide" src="/assets/img/produk-2.jpeg" alt="" srcset="">
          <div class="container h-100">
            <div class="row h-100 justify-content-end align-items-center">
              <div class="col-12 col-md-5">
                <h1 class="fw-medium ls-2">SISILIA</h1>
                <p>Terhanyutlah dalam relaksasi sisilia</p>
              </div>
            </div>
          </div>
        </div>
      <?php endfor ?>
    </div>
    <!-- If we need pagination -->
    <div class="swiper-pagination main-pagination"></div>  
    <!-- If we need navigation buttons -->
    <!-- <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div> -->  
  </div>
</section>

<section id="promotion" class="pt-0 pt-md-5 pb-5 bg-white">
  <!-- Slider main container -->
  <div class="container promo-wrapper">
    <div class="row align-items-center justify-content-between">
      <div class="col-12 col-md-3 text-white promo-text">
        <h2 class="text-uppercase fw-bold promo-text-title mt-5">Flash Sale</h2>
      </div>
      <div class="col-12 col-md-9 slide-container">
        <div class="swiper-container promo-swiper">
          <!-- Additional required wrapper -->
          <div class="swiper-wrapper">
            <!-- Slides -->
            <?php for($i = 1; $i <= 4; $i++) :?>
              <div class="swiper-slide">
                  <a href="product-detail.php" class="text-decoration-none text-dark">
                    <div class="card promo-card">
                      <div class="discount-sign">
                        <h6 class="discount">20<span>%</span></h6>
                        <p>Off</p>
                      </div>
                      <img class="img" src="/assets/img/produk-1.jpeg" class="card-img-top" alt="boboyuk" title="sisilia">
                      <div class="card-body text-center">
                        <h5 class="card-title product-title fw-normal">Milan</h5>
                        <p class="card-text product-price fw-medium">
                          <span class="lower-price text-nowrap">Rp 1.000.000</span> - 
                          <span class="lower-price text-nowrap">Rp 10.000.000</span>
                        </p>
                        <hr>
                        <div class="card-count-down d-flex flex-wrap align-items-center justify-content-center">
                          <p class="fs-m1 mb-2 mb-sm-0 me-2">Berakhir dalam:</p>
                          <div class="count-down">
                            <span class="hours"></span>
                            <span class="minutes"></span>
                            <span class="seconds"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </a>
              </div>
            <?php endfor ?>
          </div>        
        </div>
        <!-- If we need navigation buttons -->
        <div class="promo-nav-slide promo-slide-prev">
          <i class="fas fa-chevron-left bg-white"></i>
        </div>
        <div class="promo-nav-slide promo-slide-next">
          <i class="fas fa-chevron-right bg-white"></i>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="choosing-mattress">
  <div class="header">
    <div class="side left bg-primary text-white">
      <img class="img-abs img-logo" src="/assets/img/logo-boboyuk-origin-square-black.svg"></img>
      <img class="img-abs img-product" src="/assets/img/milan.png"></img>
      <div class="container">
        <div class="row justify-content-end">
          <div class="col-6">
            <h2 class="text-uppercase fw-bolder">Choosing Mattress</h2>
          </div>
        </div>
      </div>
    </div>
    <div class="side right">
      <div class="container">
        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptatibus ex facere cumque at odio numquam maiores ipsa culpa doloremque. Quod quia aspernatur sunt, in doloribus, magni laboriosam veritatis ut distinctio molestias soluta possimus, corrupti consectetur enim dicta dolorum amet facere ab laudantium! Facilis natus repellat, adipisci dolores quidem reprehenderit atque?</p>
      </div>
      <img class="img" src="/assets/img/produk-2.jpeg" alt="" srcset="" >
    </div>
  </div>
  <div class="content">
    <div class="side left">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-10">
            <h2 class="text-uppercase fw-bolder">Custom Srping Bed Anda Sendiri</h2>
            <p>Bangun spring bed anda dari berbagai macam varian kami</p>
            <form action="" action="get" class="d-grid">
              <div class="mb-3">
                <select name="springbed" class="form-select py-2 px-3 " aria-label="Default select example">
                  <option disabled selected>Pilih Ukuran</option>
                  <option value="1">Single XL (90x200cm)</option>
                  <option value="2">Queen (160x200cm)</option>
                  <option value="3">Super King (200x200cm)</option>
                  <option value="4">Double (120x200cm)</option>
                  <option value="5">King (180x200cm)</option>
                </select>
              </div>
              <button type="submit" class="btn btn-primary rounded-0">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="side right">
      <div class="container text-end">
        <img src="/assets/img/ortho-pedic-care.png" alt="" srcset="">
      </div>
    </div>
  </div>
</section>

<section id="new-products" class="bg-white py-5">
  <div class="container">
    <h2 class="ls-2 fw-normal text-center text-uppercase mb-5">New Products</h2>
    <div class="swiper-container product-swiper px-4 px-md-0">
      <!-- Additional required wrapper -->
      <div class="swiper-wrapper">
        <!-- Slides -->
        <?php for($i = 1; $i <= 3; $i++) :?>
          <div class="swiper-slide">
            <a href="product-detail.php" class="text-decoration-none text-dark">
              <div class="card product-card">
                <div class="discount-sign">
                  <h6 class="discount">20<span>%</span></h6>
                  <p>Off</p>
                </div>
                <img class="img" src="/assets/img/genoa.jpg" class="card-img-top" alt="boboyuk" title="sisilia">
                <div class="card-body text-center">
                  <h5 class="card-title product-title fw-normal">Milan</h5>
                  <p class="card-text product-price fw-medium">
                    <span class="lower-price">Rp 1.000.000</span> - 
                    <span class="lower-price">Rp 10.000.000</span>
                  </p>
                </div>
              </div>
            </a>
          </div>
        <?php endfor ?>
      </div>        
    </div>
  </div>
</section>

<section id="why-choose-us" class="py-5">
  <div class="container">
    <div class="row align-items-center justify-content-between">
      <div class="col-12 col-md-2">
        <h2 class="why-choose-us-text ls-2 text-uppercase text-center text-md-start">Mengapa <span class="text-primary fw-medium">Pilih Bobo yuk ?</span> </h2>
      </div>
      <div class="col-12 col-md-9">
        <div class="row justify-content-start justify-content-md-center">
          <div class="col-6 col-md-4 why-choose-us-item mt-3 mb-3">
            <div class="d-flex align-items-end justify-content-center">
              <h6 class="title">
                Garansi 10 Tahun
              </h6>
              <span class="icon bordered">
                <i class="fas fa-certificate fa-5x"></i>
              </span>
            </div>
          </div>
          <div class="col-6 col-md-4 why-choose-us-item mt-3 mb-3">
            <div class="d-flex align-items-end justify-content-center">
              <h6 class="title">
                Gratis Uji Coba
              </h6>
              <span class="icon">
                <i class="fas fa-bed fa-5x"></i>
              </span>
            </div>
          </div>
          <div class="col-6 col-md-4 why-choose-us-item mt-3 mb-3">
            <div class="d-flex align-items-end justify-content-center">
              <h6 class="title">
                Gratis Pengantaran
              </h6>
              <span class="icon">
                <i class="fas fa-dolly fa-5x"></i>
              </span>
            </div>
          </div>
          <div class="col-6 col-md-4 why-choose-us-item mt-3 mb-3">
            <div class="d-flex align-items-end justify-content-center">
              <h6 class="title">
                Pembayaran Mudah
              </h6>
              <span class="icon">
                <i class="fas fa-money-check-alt fa-5x"></i>
              </span>
            </div>
          </div>
          <div class="col-6 col-md-4 why-choose-us-item mt-3 mb-3">
            <div class="d-flex align-items-end justify-content-center">
              <h6 class="title">
                Angsuran
              </h6>
              <span class="icon">
                <i class="far fa-credit-card fa-5x"></i>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="about-us" class="bg-white py-5">
  <div class="container text-center">
    <div class="row justify-content-center mb-3">
      <div class="col-12 col-md-8">
        <h2 class="ls-2 fw-normal text-uppercase mb-5">Tentang Bobo yuk</h2>
        <p class="mb-3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit ipsam rem accusamus amet. Itaque soluta architecto vero corrupti ipsum omnis. Facere aperiam, incidunt molestias ducimus sed esse, expedita numquam beatae ratione rerum exercitationem eaque atque. Asperiores facere hic eaque consequatur dolorum, fugiat tempore cupiditate aliquid iste? Minus praesentium rem iste.</p>
      </div>
    </div>
    <div class="client-name fs-1">
      Boboyuk
    </div>
  </div>
</section>

<section id="brands" class="bg-white">
  <div class="brands-wrapper">
    <div class="container">
      <h2 class="ls-2 text-center fw-normal text-uppercase mb-5">Brands</h2>
      <div class="row">
        <div class="col-12">
          <div class="slide-container">
            <div class="swiper-container brands-swiper">
              <!-- Additional required wrapper -->
              <div class="swiper-wrapper">
                <!-- Slides -->
                <?php for($i = 1; $i <= 6; $i++) :?>
                  <div class="swiper-slide">
                    <img src="/assets/img/brand-black 2.png" alt="" srcset="" width="100%">
                  </div>
                <?php endfor ?>
              </div>
            </div>
            <!-- If we need navigation buttons -->
            <div class="brands-nav-slide brands-slide-prev">
              <i class="fas fa-chevron-left bg-white"></i>
            </div>
            <div class="brands-nav-slide brands-slide-next">
              <i class="fas fa-chevron-right bg-white"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="list-blog-home">
  <a href="#" class="blog-item d-flex flex-wrap text-decoration-none text-dark">
    <div class="blog-wrapper img-wrapper">
      <img class="img" src="/assets/img/produk-1.jpeg" alt="" srcset="">
    </div>
    <div class="blog-wrapper text-wrapper">
      <div class="container">
        <h5 class="title fw-bolder mb-0">Kebiasaan Tidur yang Baik Mempengaruhi Produktivitas</h5>
        <span class="date fs-m1 text-secondary">April 01, 2021</span>
        <p class="description mt-3">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor quaerat sed suscipit consequuntur distinctio quis asperiores, sunt delectus ex recusandae iusto provident perspiciatis assumenda voluptates commodi incidunt optio vitae sapiente!
        </p>
      </div>
    </div>
  </a>
</section>

<section id="instagram" class="bg-white py-5">
  <div class="container">
    <div class="row header align-items-center text-center text-md-start justify-content-center">
      <div class="col-12 col-md-auto">
        <div class="img-wrapper rounded-pill d-flex justify-content-center align-items-center m-auto mb-3 mb-md-0">
          <img src="/assets/img/logo-boboyuk-origin.svg" alt="" srcset="">
        </div>
      </div>
      <div class="col-12 col-md-auto">
        <div class="text-wrapper">
          <h5>@Boboyuk</h5>
          <p class="fs-m1">Lorem ipsum dolor sit amet.</p>
        </div>
      </div>
    </div>
    <div class="d-grid content mt-5">
      <?php for($i = 1; $i <= 8; $i++) :?>
        <div class="content-item">
          <img src="/assets/img/produk-2.jpeg" alt="" srcset="">
        </div>
      <?php endfor ?>
    </div>
  </div>
</section>

