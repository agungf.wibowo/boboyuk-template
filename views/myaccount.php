<section id="myaccount" class="pt-3 pt-sm-4 pb-5 pt-md-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6 col-lg-3 sidebar">
                <div class="user row gx-3 align-items-center">
                    <div class="col-auto col-sm-12">
                        <div class="user-img mb-3 mb-md-5">
                            <h4 class="fw-medium mb-0">A</h4>
                        </div>
                    </div>
                    <div class="col-auto col-sm-12">
                        <div class="user-text">
                            <h6 class="fw-medium">Agung Fitrah Wibowo</h6>
                            <span class="fs-m1 text-secondary">Bergabung sejak 2020</span>
                        </div>
                    </div>
                </div>
                <nav class="navbar navbar-light">
                    <div class="nav-sidebar py-4">
                        <ul class="navbar-nav nav row gy-3">
                            <li class="nav-item col-12">
                                <a href="#" class="nav-link active">
                                    <i class="fas fa-user me-2"></i>
                                    Profil
                                </a>
                            </li>
                            <li class="nav-item col-12">
                                <a href="#" class="nav-link">
                                    <i class="fas fa-heart me-2"></i>
                                    Favorit
                                </a>
                            </li>
                            <li class="nav-item col-12">
                                <a href="#" class="nav-link">
                                    <i class="fas fa-shopping-cart me-2"></i>
                                    Riwayat Pesanan
                                </a>
                            </li>
                            <li class="nav-item col-12">
                                <a href="#" class="nav-link">
                                    <i class="fas fa-envelope-open-text me-2"></i>
                                    Newsletter & Subcribe
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="col-12 col-sm-6 col-lg-9 content px-2 px-lg-5 ">
                <div class="d-flex flex-wrap align-items-center mb-3 mb-md-5">
                    <h2 class="mb-0 me-3">Informasi Profil</h2>
                    <div>
                        <button class="btn btn-sm btn-outline-primary rounded-0 btn-edit" data-bs-toggle="modal" data-bs-target="#editProfil">Edit</button>
                    </div>
                </div>
                <div class="row gy-3">
                    <div class="col-12 col-md-6">
                        <div class="row gy-3 gy-md-4">
                            <div class="col-12">
                                <span class="fs-m1 text-secondary">Nama</span>
                                <p class="fw-medium">Agung Fitrah Wibowo</p>
                            </div>
                            <div class="col-12">
                                <span class="fs-m1 text-secondary">Email</span>
                                <p class="fw-medium">agungfw@gmail.com</p>
                            </div>
                            <div class="col-12">
                                <span class="fs-m1 text-secondary">Username</span>
                                <p class="fw-medium">agungfw</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="row">
                            <div class="col-12">
                                <div class="row gy-3 gy-md-4">
                                    <div class="col-12">
                                        <span class="fs-m1 text-secondary">No.Telp/Hp</span>
                                        <p class="fw-medium">08126571889</p>
                                    </div>
                                    <div class="col-12">
                                        <span class="fs-m1 text-secondary">Tanggal Lahir</span>
                                        <p class="fw-medium">12 Agu, 1993</p>
                                    </div>
                                    <div class="col-12">
                                        <span class="fs-m1 text-secondary">Bergabung Sejak</span>
                                        <p class="fw-medium">November 15,2018</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="editProfil" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Edit Profil</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form action="" method="get">
                <div class="row gy-2">
                    <div class="col-12 col-md-6">
                        <div class="row gy-2">
                            <div class="col-12 ">
                                <label for="username" class="form-label">Username</label>
                                <input type="text" class="form-control" id="username">
                            </div>
                            <div class="col-12 ">
                                <label for="name" class="form-label">Nama</label>
                                <input type="name" class="form-control" id="name">
                            </div>
                            <div class="col-12 ">
                                <label for="noTelp" class="form-label">No Telp / HP</label>
                                <input type="tel" class="form-control" id="noTelp">
                            </div>
                            <div class="col-12 ">
                                <label for="birthdate" class="form-label">Tanggal Lahir</label>
                                <input type="text" class="form-control" id="datePicker">
                                <span id="calendar"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="row gy-2">
                            <div class="col-12 ">
                                <label for="password" class="form-label">Password</label>
                                <input type="password" class="form-control" id="password">
                            </div>
                            <div class="col-12 ">
                                <label for="confirmChangePswd" class="form-label">Konfirmasi Ganti Password</label>
                                <input type="password" class="form-control" id="confirmChangePswd">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary rounded-0" data-bs-dismiss="modal">Batal</button>
            <button type="button" class="btn btn-primary rounded-0">Simpan</button>
        </div>
        </div>
    </div>
</div>