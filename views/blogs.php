<header class="text-center py-5">
    <h1 class="text-uppercase fw-bold ls-2">
        <?= $page ?>
    </h1>
</header>
<section id="main-blog-list" class="blog-list bg-white py-5">
    <div class="container">
        <div class="row">
            <?php for($i = 1; $i <= 12; $i++) :?>
            <div class="col-6 col-sm-4 col-lg-3 g-2 g-sm-3">
                <a href="#" class="text-decoration-none text-dark">
                    <div class="card blog-item-card rounded-0 border-0">
                        <img class="img" src="/assets/img/genoa.jpg" class="card-img-top" alt="boboyuk" title="sisilia">
                        <div class="card-body px-0">
                            <h6 class="card-title fw-bolder">Kebiasaan Tidur yang Baik Mempengaruhi Produktivitas</h6>
                            <div class="card-text fw-normal">
                                <span class="fs-m1 text-secondary d-block fw-medium mb-1">April 07, 2021</span>
                                <p class="description">
                                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Odit, aspernatur?
                                </p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <?php endfor ?>
        </div>
        <div class="d-flex justify-content-center">
            <div class="col-12 col-sm-4 col-lg-3">
                <button class="btn btn-primary rounded-0 py-2 px-4 w-100">Load More</button>
            </div>
        </div>
    </div>
</section>