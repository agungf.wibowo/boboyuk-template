<section id="main-product" class="pt-3 pt-sm-4 pb-5 pt-md-5">
    <div class="container">
        <div class="row justify-content-center justify-content-md-between">
            <div class="col-12 col-md-6 mb-4">
                <!-- Slider main container -->
                <div class="swiper-container product-detail-swiper mb-3">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        <?php for($i = 1; $i <= 8; $i++) :?>
                        <div class="swiper-slide">
                            <img class="img" src="/assets/img/produk-1.jpeg" class="card-img-top" alt="boboyuk" title="sisilia" width="100%">
                        </div>
                        <?php endfor ?>
                    </div>
                </div>
                <div class="slide-container">
                    <div class="swiper-container product-detail-thumbs mx-0 mx-md-4">
                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper">
                            <!-- Slides -->
                            <?php for($i = 1; $i <= 8; $i++) :?>
                            <div class="swiper-slide">
                                <img class="img" src="/assets/img/produk-1.jpeg" class="card-img-top" alt="boboyuk" title="sisilia" width="100%">
                            </div>
                            <?php endfor ?>
                        </div>
                    </div>
            
                    <!-- If we need pagination -->
                    <div class="swiper-pagination"></div>
            
                    <!-- If we need navigation buttons -->
                    <div class="product-detail-nav-slide product-detail-slide-prev">
                        <i class="fas fa-chevron-left bg-white"></i>
                    </div>
                    <div class="product-detail-nav-slide product-detail-slide-next">
                        <i class="fas fa-chevron-right bg-white"></i>
                    </div>
            
                    <!-- If we need scrollbar -->
                    <div class="swiper-scrollbar"></div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-5">
                <h2 class="text-uppercase fw-medium promo-text-title mb-3">Sisilia</h2>
                    <div class="card-count-down mb-3 d-flex flex-wrap align-items-center justify-content-start">
                        <p class="fs-m1 mb-0 me-2">Berakhir dalam:</p>
                        <div class="count-down">
                            <span class="hours"></span>
                            <span class="minutes"></span>
                            <span class="seconds"></span>
                        </div>
                    </div>
                <form action="">
                    <p class="fw-bolder mt-3 mb-2">Set</p>
                        <div class="cus-cbox row g-2">
                            <div class="col-6 col-md-3"><input class="form-group" type="checkbox" name="fullset" id="fullset" value="Full Set"><label for="fullset">Full Set</label></div>
                            <div class="col-6 col-md-3"><input class="form-group" type="checkbox" name="matras" id="matras" value="Matras"><label for="matras">Matras</label></div>
                            <div class="col-6 col-md-3"><input class="form-group" type="checkbox" name="sandaran" id="sandaran" value="Sandaran"><label for="sandaran">Sandaran</label></div>
                            <div class="col-6 col-md-3"><input class="form-group" type="checkbox" name="divan" id="divan" value="Divan"><label for="divan">Divan</label></div>
                        </div>
                    <p class="fw-bolder mt-3 mb-2">Ukuran</p>
                    <div class="cus-radio row g-2">
                        <div class="col-12 col-md-6"><input class="form-group" type="radio" name="size" id="singleXl" value="Single XL (90 x 200 cm)"><label for="singleXl">Single XL (90 x 200 cm)</label></div>
                        <div class="col-12 col-md-6"><input class="form-group" type="radio" name="size" id="double" value="Double (120 x 200 cm)"><label for="double">Double (120 x 200 cm)</label></div>
                        <div class="col-12 col-md-6"><input class="form-group" type="radio" name="size" id="queen" value="Queen (160 x 200 cm)"><label for="queen">Queen (160 x 200 cm)</label></div>
                        <div class="col-12 col-md-6"><input class="form-group" type="radio" name="size" id="king" value="King (180 x 200 cm)"><label for="king">King (180 x 200 cm)</label></div>
                        <div class="col-12 col-md-6"><input class="form-group" type="radio" name="size" id="superking" value="Super King (200 x 200 cm)"><label for="superking">Super King (200 x 200 cm)</label></div>
                    </div>
                </form>
                <hr class="my-4 d-none d-md-block">
                <div class="add-product">
                    <div class="container px-2 px-md-0">
                        <div class="row align-items-center h-100">
                            <div class="col-6 col-md-12">
                                <div class="price bg-gray-1 px-2 py-1 py-md-2 px-md-3 border">
                                    <div class="row justify-content-between">
                                        <div class="col-12 col-md-auto">
                                            <p class="mb-2 mb-md-0 ">Harga</p>
                                        </div>
                                        <div class="col-12 col-md-auto">
                                            <div class="d-flex number flex-column flex-md-row">
                                                <p class="mb-0 discount me-3 fw-bolder fs-m1"><del>12,900,000</del></p>
                                                <p class="mb-0 total fw-bolder">10,900,000</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-md-12 mt-3">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-12 col-md">
                                        <div class="row align-items-center justify-content-between">
                                            <div class="col-auto col-md-6 col-lg-auto mb-2 mb-md-0">
                                                <p class="fw-bolder fs-m1 mb-0">Save <span class="count">20</span>%</p>
                                            </div>
                                            <div class="col-auto col-md-6 col-lg-auto mb-2 mb-md-0">
                                                <button class="btn btn-like">
                                                    <i class="fas fa-heart"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-7">
                                        <button class="btn btn-add btn-primary rounded-0 h-100 w-100">
                                            <i class="fas fa-plus fs-m1 me-1"></i> Keranjang
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="product-description" class="">
    <div class="d-flex flex-wrap description text-secondary">
        <div class="side left bg-gray-1">
            <div class="container text-center">
                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Magnam, labore maxime a reprehenderit fuga neque dignissimos aliquid doloremque veritatis, perspiciatis nobis pariatur rem facilis. Ex quisquam similique amet a obcaecati labore optio eaque doloribus saepe libero iste veniam, facilis unde debitis deleniti, laborum commodi aliquam voluptatum reiciendis assumenda? Rerum, ex.</p>
            </div>
        </div>
        <div class="side right">
            <div class="container">
                <div class="w-100">
                    <p>
                        <table class="adv-table w-100">
                            <tr>
                                <td>Collection</td>
                                <td>:</td>
                                <td>Modern Living Series</td>
                            </tr>
                            <tr>
                                <td>Comfort Level</td>
                                <td>:</td>
                                <td>Medium, Soft</td>
                            </tr>
                            <tr>
                                <td>Mattress Thickness</td>
                                <td>:</td>
                                <td>38 cm</td>
                            </tr>
                            <tr>
                                <td>Headboard Type</td>
                                <td>:</td>
                                <td>Basilica</td>
                            </tr>
                            <tr>
                                <td>Foundation Type</td>
                                <td>:</td>
                                <td>Basilica</td>
                            </tr>
                        </table>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex flex-wrap advantage">
        <div class="side left bg-primary text-white">
            <div class="container">
                <ul class="row nav justify-content-center mb-5" id="myTab" role="tablist">
                    <li class="col-auto" role="presentation">
                        <button class="btn active" id="adv1-tab" data-bs-toggle="tab" data-bs-target="#adv1" type="button" role="tab" aria-controls="adv1" aria-selected="true"><i class="fas co fa-stroopwafel text-white"></i></button>
                    </li>
                    <li class="col-auto" role="presentation">
                        <button class="btn" id="adv2-tab" data-bs-toggle="tab" data-bs-target="#adv2" type="button" role="tab" aria-controls="adv2" aria-selected="false"><i class="fas co fa-swatchbook text-white"></i></button>
                    </li>
                </ul>
                <div class="tab-content text-center" id="myTabContent">
                    <div class="tab-pane fade show active" id="adv1" role="tabpanel" aria-labelledby="adv1-tab">
                        <h4 class="text-uppercase fw-normal">Premium Kit</h4>
                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Magnam, labore maxime a reprehenderit fuga neque dignissimos aliquid doloremque veritatis, perspiciatis nobis pariatur rem facilis. Ex quisquam similique amet a obcaecati labore optio eaque doloribus saepe libero iste veniam, facilis unde debitis deleniti, laborum commodi aliquam voluptatum reiciendis assumenda? Rerum, ex.</p>
                    </div>
                    <div class="tab-pane fade" id="adv2" role="tabpanel" aria-labelledby="adv2-tab">
                        <h4 class="text-uppercase fw-normal">Latex Layer</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio omnis praesentium aspernatur, voluptatum at esse quam blanditiis id libero maxime perferendis quo tempore nesciunt. Ex et veritatis eum unde incidunt? Lorem ipsum dolor sit amet consectetur adipisicing elit. At porro modi magnam, recusandae temporibus debitis, magni excepturi hic ducimus voluptatibus fugiat laboriosam ab architecto quasi sed perferendis delectus, dicta dolorum! Lorem ipsum dolor, sit amet consectetur adipisicing elit. Excepturi ad eligendi ducimus praesentium vero illo? Eaque nulla omnis minus assumenda nihil dolorem ad, blanditiis incidunt quam doloremque? Illo, fugiat deserunt!</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="side right">
            <img class="img" src="/assets/img/produk-2.jpeg" alt="" srcset="">
        </div>
    </div>
</section>

<section id="review" class="bg-white py-5">
    <div class="container">
        <h2 class="ls-2 fw-normal text-center text-uppercase mb-5">Ulasan Produk</h2>
        <div class="total-review text-center mb-5">
            <h2 class="total fw-normal mb-3 display-4">4.5 / 5</h2>
            <div class="stars text-warning mb-3 fs-5">
                <i class="fas fa-star mx-1"></i>
                <i class="fas fa-star mx-1"></i>
                <i class="fas fa-star mx-1"></i>
                <i class="fas fa-star mx-1"></i>
                <i class="fas fa-star-half-alt mx-1"></i>
            </div>
            <span class="reviewed fs-m1">5 Ulasan</span>
        </div>
        <div class="swiper-container review-swiper pe-4 pe-md-5">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <!-- Slides -->
            <?php for($i = 1; $i <= 5; $i++) :?>
            <div class="swiper-slide">
                <a href="#" class="text-decoration-none text-dark">
                <div class="card review-card">
                    <div class="card-body d-flex flex-wrap">
                        <div class="header d-flex">
                            <div class="user-img me-2">
                                <h4 class="fw-medium m-0">Y</h4>
                            </div>
                            <div class="user-text">
                                <h6 class="mb-1">Yosua</h6>
                                <div class="stars text-warning">
                                    <i class="fas fa-star me-0 me-md-1"></i>
                                    <i class="fas fa-star me-0 me-md-1"></i>
                                    <i class="fas fa-star me-0 me-md-1"></i>
                                    <i class="fas fa-star me-0 me-md-1"></i>
                                    <i class="fas fa-star me-0 me-md-1"></i>
                                </div>
                            </div>
                        </div>
                        <p class="card-text review-text fs-m1">
                            Kualitas barang yg luar biasa, Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores, laborum?
                        </p>
                    </div>
                </div>
                </a>
            </div>
            <?php endfor ?>
        </div>        
        </div>
    </div>
</section>

<section id="similar-products" class="bg-white py-5">
    <div class="container">
        <h2 class="ls-2 fw-normal text-center text-uppercase mb-5">Produk Serupa</h2>
        <div class="swiper-container product-swiper pe-4 pe-md-0">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <!-- Slides -->
            <?php for($i = 1; $i <= 3; $i++) :?>
            <div class="swiper-slide">
                <a href="product-detail.php" class="text-decoration-none text-dark">
                <div class="card product-card">
                    <div class="discount-sign">
                        <h6 class="discount">20<span>%</span></h6>
                        <p>Off</p>
                    </div>
                    <img class="img" src="/assets/img/genoa.jpg" class="card-img-top" alt="boboyuk" title="sisilia">
                    <div class="card-body text-center">
                        <h5 class="card-title product-title fw-normal">Milan</h5>
                        <p class="card-text product-price fw-medium">
                            <span class="lower-price">Rp 1.000.000</span> - 
                            <span class="lower-price">Rp 10.000.000</span>
                        </p>
                    </div>
                </div>
                </a>
            </div>
            <?php endfor ?>
        </div>        
        </div>
    </div>
</section>