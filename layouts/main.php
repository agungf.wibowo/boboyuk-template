<?php
    $products = [
        'Milan',
        'Livorno',
        'Sisilia',
        'San Pietro',
        'Chiro Care',
        'Masseria',
        'Orthopedic Care',
        'Genoa',
        'Regina',
        'Reggina',
        'Sinna',
        'Piacenza',
    ]
?>

<!DOCTYPE html>
<html lang="en">

    <?php include 'components/head.php' ?>

    <body>
        <?php include 'components/navbar.php' ?>
        
        <?php include 'setpage.php' ?>
        
        <?php include 'components/footer.php' ?>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

        <!-- <script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
        <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script> -->
        <script type="module" src="/assets/js/myswiper.js"></script>
        <script type="module" src="/assets/js/calendar.js"></script>
        <script src="/assets/js/custom.js"></script>
        
    </body>

</html>