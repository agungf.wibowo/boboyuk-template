<?php
    switch($page){
        case 'home': // $page == home (jika isi dari $page adalah home)
        include "views/home.php"; // load file home.php yang ada di folder views
        break;
        
        case 'blogs': // $page == blog (jika isi dari $page adalah blog)
        include "views/blogs.php"; // load file blog.php yang ada di folder views
        break;
        
        case 'category': // $page == about (jika isi dari $page adalah about)
        include "views/category.php"; // load file about.php yang ada di folder views
        break;

        case 'product detail': // $page == about (jika isi dari $page adalah about)
        include "views/product-detail.php"; // load file about.php yang ada di folder views
        break;

        case 'my account': // $page == about (jika isi dari $page adalah about)
        include "views/myaccount.php"; // load file about.php yang ada di folder views
        break;

        case 'about': // $page == about (jika isi dari $page adalah about)
        include "views/about.php"; // load file about.php yang ada di folder views
        break;
        
        // case 'case_selanjutnya':
        // include "views/case_selanjutnya.php";
        // break;
        
        // case 'case_selanjutnya':
        // include "views/case_selanjutnya.php";
        // break;
        
        // case 'case_selanjutnya':
        // include "views/case_selanjutnya.php";
        // break;
        
        default: // Ini untuk set default jika isi dari $page tidak ada pada 3 kondisi diatas
        include "views/home.php";
    }
?>