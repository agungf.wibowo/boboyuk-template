import Swiper from '/assets/js/swiper-bundle.esm.browser.min.js'

const mainSlider = new Swiper('.main-swiper', {
    // Optional parameters
    loop: true,
    // If we need pagination
    pagination: {
        el: '.main-pagination',
        dynamicBullets: true,
        clickable : true
    },
    effect : 'slide',
    autoplay : {
        duration : 2000,
    },
    grabCursor : true,
});

const promoSlider = new Swiper('.promo-swiper', {
    // Optional parameters
    // loop: true,
    slidesPerView: 2,
    spaceBetween: 10,
    effect : 'slide',
    grabCursor : true,
    navigation: {
        nextEl: '.promo-slide-next',
        prevEl: '.promo-slide-prev',
    },
    // Responsive breakpoints
    breakpoints: {
      // when window width is >= 576px
      576: {
            slidesPerView: 2,
            spaceBetween: 20
        },
        992: {
            slidesPerView: 2,
            spaceBetween: 40
        },
        1200: {
            slidesPerView: 3,
            spaceBetween: 30
        },
    }
});

const brandSlider = new Swiper('.brands-swiper', {
    // Optional parameters
    loop: true,
    slidesPerView: 2,
    spaceBetween: 10,
    effect : 'slide',
    grabCursor : true,
    // If we need navigationS
    navigation: {
        nextEl: '.brands-slide-next',
        prevEl: '.brands-slide-prev',
    },
    autoplay : {
        duration : 2000,
    },
    breakpoints: {
      // when window width is >= 576px
        576: {
            slidesPerView: 3,
            spaceBetween: 20,
        },
        992: {
            slidesPerView: 4,
            spaceBetween: 40,
        },
        1200: {
            slidesPerView: 5,
            spaceBetween: 30,
        },
    }
});


const productDetailThumbs = new Swiper('.product-detail-thumbs', {
    // Optional parameters
    spaceBetween: 10,
    slidesPerView: 4,
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
});

const productDetailSlider = new Swiper('.product-detail-swiper', {
    // Optional parameters
    spaceBetween: 10,
    zoom: true,
    navigation: {
        nextEl: '.product-detail-slide-next',
        prevEl: '.product-detail-slide-prev',
    },
    thumbs: {
        swiper: productDetailThumbs
    }
});

const productSlider = new Swiper('.product-swiper', {
    // Optional parameters
    spaceBetween: 10,
    slidesPerView: 2,
    grabCursor : true,
    breakpoints: {
      // when window width is >= 576px
        768: {
            slidesPerView: 3,
            spaceBetween: 20,
        },
    }
});

const ratingSlider = new Swiper('.review-swiper', {
    // Optional parameters
    spaceBetween: 10,
    slidesPerView: 2,
    grabCursor : true,
    breakpoints: {
      // when window width is >= 576px
        768: {
            slidesPerView: 3,
            spaceBetween: 20,
        },
    }
});
