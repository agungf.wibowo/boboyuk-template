let countDownDate = new Date("Jan 5, 2022 15:37:25").getTime();

// Update the count down every 1 second

window.addEventListener('load', function () {
    setInterval(function() {
    
        // Get today's date and time
        let now = new Date().getTime();
    
        // Find the distance between now and the count down date
        let distance = countDownDate - now;
        
        // Time calculations for days, hours, minutes and seconds
        let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        let seconds = Math.floor((distance % (1000 * 60)) / 1000);
        
        // Display the result in the element with id="demo"
        let elLength = document.querySelectorAll(".count-down .hours")
        
        elLength.forEach((el, i) => {
            document.querySelectorAll(".count-down .hours")[i].innerHTML = (hours < 10) ? "0"+hours : hours;
            document.querySelectorAll(".count-down .minutes")[i].innerHTML = (minutes < 10) ? "0"+minutes : minutes;
            document.querySelectorAll(".count-down .seconds")[i].innerHTML = (seconds < 10) ? "0"+seconds : seconds;
        });
        
        // If the count down is finished, write some text
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("demo").innerHTML = "EXPIRED";
        }
    }, 1000);

})

const btnLike = document.querySelector('.btn-like');
const addProduct = document.querySelector('.add-product');


btnLike.addEventListener('click', function (e) {
    (e.target).classList.toggle('liked');
});


var prevScrollpos = window.pageYOffset;
let gap = 0;
window.onscroll = function(e) {
var currentScrollPos = window.pageYOffset;
if (prevScrollpos < currentScrollPos) {
        gap ++
        if(gap > 20) {
            addProduct.style.bottom = "0";
            gap = 0
        }
    } else {
        addProduct.style.bottom = "-100%";
    }
    prevScrollpos = currentScrollPos;
}

var dateComponent = new DatePicker({
    el: document.querySelector('#calendar'),
    onchange: function (curr) {
        dateInput.value = curr;
    }
});

var dateInput = document.querySelector('#datePicker');

dateInput.onfocus = function () {
    dateComponent.show();
};