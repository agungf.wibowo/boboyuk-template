<footer class="bg-primary text-white">
    <div class="container py-5">
        <div class="row">
            <div class="col-12 col-md-2">
                <img src="/assets/img/logo-boboyuk-origin-square.svg" alt="" srcset="" width="70">
                <h5 class="">Boboyuk</h5>
            </div>
            <div class="col-12 col-md-10">
                <h4 class="fw-bolder mt-4 mt-md-0 mb-3">Model</h4>
                <div class="row">
                    <div class="col-12 col-md-5 devider">
                        <div class="row">
                            <?php foreach($products as $key => $product) : ?>
                                <div class="col-12 col-sm-6">
                                    <a class="mb-2" href="#"><?= $product ?></a>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <div class="col-12 col-md-7">
                    <hr class="d-block d-md-none">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="px-0 px-md-5 mb-2 mb-md-0">
                                    <div class="row">
                                        <div class="col-12">
                                            <a class="mb-2" href="#">FAQ</a>
                                        </div>
                                        <div class="col-12">
                                            <a class="mb-2" href="#">Contact US</a>
                                        </div>
                                        <div class="col-12">
                                            <a class="mb-2" href="#">About Us</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="row">
                                    <div class="col-12 mb-3">
                                    <a class="btn-sosmed me-2" href="#">
                                        <i class="fab fa-twitter  bg-white text-primary"></i>
                                    </a>
                                    <a class="btn-sosmed me-2" href="#">
                                        <i class="fab fa-instagram bg-white text-primary"></i>
                                    </a>
                                    </div>
                                    <div class="col-12">
                                        <div class="d-flex mb-2 mb-sm-0">
                                            <i class="fas fa-phone-alt me-3 m-icon"></i>
                                            <p>+62 624 0265 0226</p>
                                        </div>
                                        <div class="d-flex">
                                            <i class="fas fa-map-marker-alt me-3 m-icon"></i>
                                            <p> Kasuari No. A1 Komplek Anggrek,<br>Medan Area, 20129<br>Medan, Sumatera Utara, Indonesia</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>