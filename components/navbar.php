<nav class="navbar navbar-expand-lg navbar-light bg-white">
  <div class="container">
    <div class="d-flex flex-wrap justify-content-between align-items-center w-100">
      <div class="col-auto d-flex align-items-center">
        <a class="navbar-brand" href="#">
          <img src="/assets/img/logo-boboyuk-origin.svg" alt="" srcset="" width="100px">
        </a>
      </div>
      <div class="col-auto col-lg-auto order-lg-2">
        <div class="d-flex w-100 justify-content-end">
          <ul class="navbar-nav align-items-center flex-row">
            <li class="nav-item dropdown account me-2">
              <a class="nav-link dropdown-toggle <?php echo isActive('myaccount') ? 'active' : '' ?>" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              <div class="d-inline-block">
                <div class="d-flex align-items-center">
                  <div class="user-img me-2">
                    <h4 class="fw-medium mb-0">A</h4>
                  </div>
                  <h6 class="user-name mb-0">Agung</h6>
                </div>
              </div>
              </a>
              <ul class="dropdown-menu cus-dropdown-menu px-2" aria-labelledby="myaccount">
                <li><a class="dropdown-item" href="#">Profil</a></li>
                <li><a class="dropdown-item" href="#">Favorit</a></li>
                <li><a class="dropdown-item" href="#">Riwayat Pemesanan</a></li>
                <li><a class="dropdown-item" href="#">Newsletter & Subcribe</a></li>
                <li><hr class="dropdown-divider"></li>
                <li><a class="dropdown-item" href="#">Sign out</a></li>
              </ul>
            </li>
            <li class="nav-item nav-cart align-items-center">
              <a class="nav-link px-2 <?php echo isActive('cart') ? 'active' : '' ?>" href="/cart.php">
                <div class="wrapper">
                  <i class="fas fa-shopping-cart text-secondary"></i>
                    <span class="badge bg-primary">4</span>
                </div>
              </a>
            </li>
          </ul>
          <button class="navbar-toggler border-0 rounded-0 px-2 ms-2 d-flex d-lg-none align-items-center" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        </div>
      </div>
      <div class="col-12 col-lg order-lg-1">
        <div class="collapse navbar-collapse justify-content-end" id="navbarTogglerDemo02">
          <ul class="navbar-nav mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link <?php echo isActive('Home') ? 'active' : '' ?>" aria-current="page" href="/">Home</a>
            </li>
            <li class="nav-item dropdown has-megamenu">
              <a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown"> Spring Bed </a>
              <div class="dropdown-menu megamenu" role="menu">
                <div class="container my-3">
                  <h4 class="fw-medium">Produk Keategori</h4>
                  <div class="row">
                    <?php foreach($products as $key => $product) : ?>
                      <div class="col-12 col-sm-6 col-md-4">
                        <a class="nav-link <?php echo isActive('Category') ? 'active' : '' ?>" href="category.php"><?= $product ?></a>
                      </div>
                    <?php endforeach ?>
                  </div>
                </div>
              </div> <!-- dropdown-mega-menu.// -->
            </li>
            <li class="nav-item">
              <a class="nav-link <?php echo isActive('Blogs') ? 'active' : '' ?>" href="/blogs.php">Blog</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</nav>